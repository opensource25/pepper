import json
from this import d
import time
import pysher
import logging
import threading
import sys
import websocket
#import rel
from pepper_package import pepper

#pepper = pepper.Pepper("/home/bjarne/pepper/config.toml")

appkey = "95be49615b187f7f424a"
secret = "e2c105726b804af2054d"

#threading.Thread(target=pepper.showWeb, args=("https://api.stimm-development.de/ZKW/home.html", 10,)).start()

def StrToDict(string):
    return json.loads(string)

def speak(data):
    message = data.get('message')
    print(message)
    pepper.say(message)

def on_message(ws, message):
    print(message)
    print(type(message))
    args = StrToDict(message)
    print(type(args))
    print(args)
    print('channel' in message)
    channel = args.get('channel')
    if channel == "tts":
        speak(StrToDict(args.get('data')))
    else:
        print(message)

def on_error(ws, error):
    print(error)

def on_close(ws, close_status_code, close_msg):
    print("### closed ###")

def on_open(ws):
    print("Opened connection")
    ws.send('{"event":"pusher:subscribe", "data":{"channel": "tts"}}') 

websocket.enableTrace(True)
ws = websocket.WebSocketApp("wss://ws-eu.pusher.com:443/app/"+appkey+"?protocol=5&flash=false&client=js&version=7.0.3",
                            on_open=on_open,
                            on_message=on_message,
                            on_error=on_error,
                            on_close=on_close)

ws.run_forever()  # Set dispatcher to automatic reconnection
# rel.signal(2, rel.abort)  # Keyboard Interrupt
# rel.dispatch()

# threading.Thread(target=pepper.showWeb, args=("https://api.stimm-development.de/ZKW/home.html", -1,)).start()
# if input("enter to exit:"):
#  exit()