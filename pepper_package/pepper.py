import qi
import sys
import toml
import time
import math

class Pepper():
    def __init__(self, configfile):
        config = toml.load(configfile)
        ip = config['connection']['ip']
        port = config['connection']['port']
        self.session = qi.Session()
        try:
            self.session.connect("tcp://" + ip + ":" + str(port))
        except RuntimeError:
            print ("Can't connect to Naoqi at ip \"" + ip + "\" on port " + str(port) +".\n"
                    "Please check your script arguments. Run with -h option for help.")
            sys.exit(1)

        # creating Services
        self.dialog_service = self.session.service("ALDialog") # - wahrscheinlich nicht existent!
        self.state_service = self.session.service("ALAutonomousLife")
        self.state_service = self.session.service("ALAutonomousLife")
        self.posture_service = self.session.service("ALRobotPosture")
        self.motion_service = self.session.service("ALMotion")
        self.navigation_service = self.session.service("ALNavigation")
        self.tts_service = self.session.service("ALTextToSpeech")
        self.speechrecognition_service = self.session.service("ALSpeechRecognition")
        self.tablet_service = self.session.service("ALTabletService")
        self.audio_player_service = self.session.service("ALAudioPlayer")
        self.recharge_service = self.session.service("ALRecharge")
        self.battery_service = self.session.service("ALBattery")

    def setState(self, state):
        # set disabled(autonomouslife off), solitary, interactive, safeguard
        self.state_service.setState(state)

    def getState(self):
        # print state
        state = self.state_service.getState()
        print ('Peppers aktueller Status ist: "%s"') % state

    def say(self, text, language="German"):
        # sage Text
        self.tts_service.say(text)

    def standUpright(self):
        # stand Upright
        self.posture_service.goToPosture("StandInit", 1.0)

    def setPosture(self, posture):
        self.posture_service.goToPosture(posture, 1.0)

    def stopMoving(self):
        self.motion_service.stopMove()

    def moveByRelativCordinates(self, x=0, z=0, rotation=0):
        # wakeUp pepper
        self.motion_service.wakeUp()
        # stand Upright
        self.standUpright()
        # convert possible ints to floats
        x = float(x)
        z = float(z)
        rotation = float(rotation)
        # move x = forwards, -x = backwards, y = left, -y = right, 1 * pi = 180Grad nach links, -1 * pi = 180Grad nach rechts 
        self.motion_service.moveTo(x, z, rotation * math.pi)
        # stand Upright don't frogot this
        self.standUpright()

    def navitageByRelativCordinates(self, x=0, z=0):
        # wakeUp pepper
        self.motion_service.wakeUp()
        # stand Upright
        self.standUpright()
        # convert possible ints to floats
        x = float(x)
        z = float(z)
        # move x = forwards, -x = backwards, y = left, -y = right
        self.navigation_service.navigateTo(x, z)
        # stand Upright don't frogot this
        self.standUpright()

    def setLanguage(self, language):
        self.dialog_service.setLanguage(language)
        # self.tts_service.setLanguage(language)
        # self.speechrecognition_service.setLanguage(language)

    def showImage(self, picture_url, duaration):
        try:
            # Display a local image located in img folder in the root of the web server
            # The ip of the robot from the tablet is 198.18.0.1
            self.tablet_service.showImage(picture_url)
            time.sleep(duaration)
            # Hide the web view
            self.tablet_service.hideImage()
        except Exception, e:
            print "Error was: ", e

    def showWeb(self, url, duaration):
        if duaration < 0:
            duaration = 99999
        try:
            self.tablet_service.showWebview(url)
            time.sleep(duaration)
        finally:
            self.tablet_service.hideWebview()

    def hideWeb(self):
        self.tablet_service.hideWebview()

    def playAudio(self, audio_path, _async=False): #audio-file must be absolut and on pepper
        file_id = self.audio_player_service.loadFile(audio_path)
        
        self.audio_player_service.play(file_id, _async=_async)

    def activateDialog(self, dialog_file):
        top_path = dialog_file.decode('utf-8')
        top_name = self.dialog_service.loadTopic(top_path.encode('utf-8'))

        self.dialog_service.activateTopic(top_name)

        self.dialog_service.subscribe('my_dialog_example')

        try:
            raw_input("\nSpeak to the robot using rules from the just loaded .top file. Press Enter when finished:")
        finally:
            # stopping the dialog engine
            self.dialog_service.unsubscribe('my_dialog_example')

            # Deactivating the topic
            self.dialog_service.deactivateTopic(top_name)

            # now that the dialog engine is stopped and there are no more activated topics,
            # we can unload our topic and free the associated memory
            self.dialog_service.unloadTopic(top_name)

    def goToStation(self, fine_control=True):
        self.motion_service.wakeUp()

        if fine_control:
            self.say("Ich suche jetzt meine Station")
            found = self.recharge_service.lookForStation()
            if found[0] != 0: # The charging station has not been found.
                print "Station is not found."
                self.say("Ich konnte die Station leider nicht finden")
                return
            self.say("Ich sehe meine Station und fahre jetzt zu ihr")
            # Move in front of charging station using ALTracker
            correct = self.recharge_service.moveInFrontOfStation()
            if correct != 0: # The robot has not been able to move close to the charging station.
                print "Unable to move in front of the station."
                self.say("Ich konnte mich leider nicht vor meine Station stellen")
                return
            self.say("Ich befinde mich an meine Station und docke jetzt an")
            # Start docking motion
            docked = self.recharge_service.dockOnStation()
            if docked != 0: # The docking failed.
                print "Unable to dock on the charging station."
                self.say("Ich konnte nich an meine Station andocken")
                return
            print "Robot successfully docked."
            self.say("Ich habe an meiner Station erfolgreich angedockt")
        else:
            self.say("Ich begebe mich jetzt auf die Suche nach meine Station")
            success = self.recharge_service.goToStation()
            if success != 0: # The charging station has not been found.
                print "Station is not found."
                self.say("Ich konnte die Station leider nicht finden")
                return
            print "Robot successfully docked."
            self.say("Ich habe an meiner Station erfolgreich angedockt")
    
    def leaveStation(self):
        self.say("Ich verlasse meine Station wieder")
        self.recharge_service.leaveStation()
        print "Robot successfully docked off."
        self.say("Ich habe meine Station wieder verlassen")

    def checkBatteryStatus(self, vocal_output=False):
        status = self.battery_service.getBatteryCharge()
        print "Mein Akku hat noch: " + status + "%"
        if vocal_output:
            self.say("Mein Akku hat noch: " + status + "Prozent")
        return status

    def enableSelfLoading(self):
        status = self.checkBatteryStatus()
        if status <= 10:
            self.say("Mein Akku hat nur noch " + status + "%.")
            print "Robot need to charge"
            self.goToStation()